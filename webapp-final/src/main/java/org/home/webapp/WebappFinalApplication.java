package org.home.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebappFinalApplication /* extends SpringBootServletInitializer */ {

//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(WebappFinalApplication.class);
//    }

    public static void main(String[] args) {
        SpringApplication.run(WebappFinalApplication.class, args);
    }
}
