# README #

### What is this repository for? ###

* Quick summary: Just try to play with technology stack:

### BE ###

* Spring Security 
* JMS 
* Multithreading: akka, ... 
* Maven modules with Spring boot 
* Flyway / Liquebase / ..?.. 
* Cache
* Big Data - Hadoop, ??..

### FE ###

* Spring MVC FE 
* Templating : thymeleaf vs freemarker vs velocity FE
* Meteor... FE
* Webjars FE

### Other: ###

* Mobile version: Phone GAP / Cordova ME
* Docker DO
*- CRaSH 
* Metrics 
* Continues Delivery? ..
* Spring boot 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact