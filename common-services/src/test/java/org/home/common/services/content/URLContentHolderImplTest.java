package org.home.common.services.content;

import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by obaliev on 6/21/15.
 */
public class URLContentHolderImplTest {

    URLContentHolder URLContentHolder;

    @Test
    public void testSimple() throws Exception {

        InputStream inputStream = URLContentHolder.openInputStream(new URL("http://sudakflat.info/"));

        Assert.assertNotNull(inputStream);
    }
}
