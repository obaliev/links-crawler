package org.home.common.services.measure;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import org.hamcrest.Matchers;
import org.home.common.services.util.measure.TimeMeasurer;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.util.StopWatch;

import java.util.HashMap;

/**
 * Created by obaliev on 6/14/15.
 */
public class TestMeasureApproaches {

    private final static String MIN = "MIN";
    private final static String MAX = "max";
    private final static String SUM = "sum";

    @Test
    public void testNanoMeasurerWithTimerReport() throws Exception {
        TimeMeasurer measurer = new TimeMeasurer();

        measurer.start();
        measurer.measure("simple");

        measurer.measure("simple2");

        measurer.measure("simple3");
        measurer.measure("simple");
        measurer.measure("simple4");
        measurer.measure("simple3");
        measurer.measure("simple");
        measurer.measure("simple");
        measurer.measure("simple4");
        measurer.measure("simple");
        measurer.measure("simple3");
        measurer.measure("simple");

        measurer.measure("simple2");
        measurer.measure("simple2");
        measurer.measure("simple2");
        measurer.measure("simple2");
        measurer.measure("simple2");

        measurer.stop();

        System.out.println(measurer.printReportForMeasureTypes("simple"));
        System.out.println(measurer.printReportForMeasureTypes("simple2"));
        System.out.println(measurer.printReportForMeasureTypes("simple3"));
        System.out.println(measurer.printReportForMeasureTypes("simple4"));
        System.out.println(measurer.printReportForMeasureTypes("simple5"));
    }

    @Test
    public void testNanoMeasurer() throws Exception {
        TimeMeasurer measurer = new TimeMeasurer();
        measurer.start();
        System.out.println("Sout");
        measurer.stop();
        System.out.println("nanoTime: Sout was shown in: " + measurer.getStopStartDiffTime());
    }

    @Test
    public void testMeasurerItselfTimeAverage() throws Exception {
        HashMap<String, Long> start = new HashMap<>();
        HashMap<String, Long> measure = new HashMap<>();
        HashMap<String, Long> measureAndPrint = new HashMap<>();
        HashMap<String, Long> measureAndPrintWithPrefixMessage = new HashMap<>();
        HashMap<String, Long> stop = new HashMap<>();

        int count = 5000000;

        for (int i = 0; i < count; ++i) {
            TimeMeasurer timeMeasurer = new TimeMeasurer();
            long startTime = System.nanoTime();
            timeMeasurer.start();
            long startEndTime = System.nanoTime();
            calculateMinMaxSum(start, startEndTime - startTime);

            startTime = System.nanoTime();
            timeMeasurer.measure();
            startEndTime = System.nanoTime();
            calculateMinMaxSum(measure, startEndTime - startTime);

            startTime = System.nanoTime();
            timeMeasurer.measureAndPrint();
            startEndTime = System.nanoTime();
            calculateMinMaxSum(measureAndPrint, startEndTime - startTime);

            startTime = System.nanoTime();
            timeMeasurer.measureAndPrint("Prefix mesage: ");
            startEndTime = System.nanoTime();
            calculateMinMaxSum(measureAndPrintWithPrefixMessage, startEndTime - startTime);

            startTime = System.nanoTime();
            timeMeasurer.stop();
            startEndTime = System.nanoTime();
            calculateMinMaxSum(stop, startEndTime - startTime);
        }

        System.out.println(printMinMaxSumReport(start, "start() - ", count, 200));
        System.out.println(printMinMaxSumReport(measure, "stop() - ", count, 200));
        System.out.println(printMinMaxSumReport(measureAndPrint, "measureAndPrint() - ", count, 320));
        System.out.println(printMinMaxSumReport(measureAndPrintWithPrefixMessage, "measureAndPrintWithPrefixMessage() - ", count, 400));
        System.out.println(printMinMaxSumReport(stop, "stop() - ", count, 200));

    }

    private String printMinMaxSumReport(HashMap<String, Long> meauseredValues, String prefixString, int count, long averageThresholder) {
        long average = meauseredValues.get(SUM) / count;

        Assert.assertThat("Average time: " + averageThresholder + ",  for " + prefixString + " is more then thresholder: " + averageThresholder,
                average, Matchers.lessThanOrEqualTo(averageThresholder));

        return prefixString + "min: " + meauseredValues.get(MIN) + ", max: " + meauseredValues.get(MAX)
                + ", average: " + average;
    }

    private void calculateMinMaxSum(HashMap<String, Long> meauseredValues, long time) {
        if (meauseredValues.get(MIN) == null || meauseredValues.get(MIN) > time) {
            meauseredValues.put(MIN, time);
        }

        if (meauseredValues.get(MAX) == null || meauseredValues.get(MAX) < time) {
            meauseredValues.put(MAX, time);
        }

        if (meauseredValues.get(SUM) == null) {
            meauseredValues.put(SUM, time);
        } else {
            meauseredValues.put(SUM, meauseredValues.get(SUM) + time);
        }
    }

    @Test
    public void testMeasurerItselfTime() throws Exception {
        TimeMeasurer timeMeasurer = new TimeMeasurer();
        long startTime = System.nanoTime();
        timeMeasurer.start();
        long startEndTime = System.nanoTime();
        System.out.println("Nano measurer start() time: " + (startEndTime - startTime));

        startTime = System.nanoTime();
        timeMeasurer.measure();
        startEndTime = System.nanoTime();
        System.out.println("Nano measurer stop() time: " + (startEndTime - startTime));

        startTime = System.nanoTime();
        timeMeasurer.measureAndPrint();
        startEndTime = System.nanoTime();
        System.out.println("Nano measurer measureAndPrint() time: " + (startEndTime - startTime));

        startTime = System.nanoTime();
        timeMeasurer.measureAndPrint("Prefix mesage: ");
        startEndTime = System.nanoTime();
        System.out.println("Nano measurer measureAndPrint(\"Prefix mesage: \") time: " + (startEndTime - startTime));

        startTime = System.nanoTime();
        timeMeasurer.stop();
        startEndTime = System.nanoTime();
        System.out.println("Nano measurer stop() time: " + (startEndTime - startTime));
    }

    @Test
    public void testNanoTime() throws Exception {
        long startTime = System.nanoTime();
        System.out.println("Sout");
        long endTime = System.nanoTime();

        System.out.println("nanoTime: Sout was shown in: " + (endTime - startTime));
    }

    @Test
    public void testMillieSec() throws Exception {
        long startTime = System.currentTimeMillis();
        System.out.println("Sout");
        long endTime = System.currentTimeMillis();

        System.out.println("currentTimeMillis: Sout was shown in: " + (endTime - startTime));
    }

    @Test
    public void testStopWatch() throws Exception {
        StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        System.out.println("Sout");
        stopWatch.stop();

        System.out.println("StopWatch: Sout was shown in: " + stopWatch.prettyPrint());
    }

    @Test
    public void testJamon() throws Exception {
        Monitor jaMon = MonitorFactory.start();

        jaMon.start();
        System.out.println("Sout");
        jaMon.stop();

        System.out.println("Jamon: Sout was shown in: " + jaMon);
    }


}
