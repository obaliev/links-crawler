package org.home.common.services.util.measure;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by obaliev on 6/22/15.
 */
public class MultipleTimeMeasurerTest {

    MultipleTimeMeasurer multipleTimeMeasurer = new MultipleTimeMeasurer();

    @Test
    public void testMeasureStopAndStart() throws Exception {
        multipleTimeMeasurer.start();
        multipleTimeMeasurer.stop();

        multipleTimeMeasurer.start();
        multipleTimeMeasurer.stopAndStart();
        multipleTimeMeasurer.stop();

        Assert.assertThat(multipleTimeMeasurer.getMeasures().size(), Matchers.is(3));
        for (SimpleTimeMeasurer timeMeasurer : multipleTimeMeasurer.getMeasures()) {
            Assert.assertThat(timeMeasurer.getDiff(), Matchers.greaterThan(0l));
        }
    }
}