package org.home.common.services.content;

import org.home.common.services.content.domain.BasicPageResult;
import org.junit.Test;
import org.springframework.util.Assert;

import java.net.URL;

/**
 * Created by obaliev on 6/21/15.
 */
public class BasicRegExpContentProcessorTest {

    BasicRegExpContentProcessor basicRegExpProcessor = new BasicRegExpContentProcessor();

    @Test
    public void testProcessInputStream() throws Exception {
        BasicPageResult basicPageResult = basicRegExpProcessor.processInputStream(new URL("http://sudakflat.info/").openStream());

        Assert.notNull(basicPageResult);
        Assert.notEmpty(basicPageResult.getHrefSet());
    }
}