package org.home.common.services.link.parsers;

import org.home.common.services.util.measure.TimeMeasurer;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by obaliev on 6/14/15.
 */
public class TestLinksRetireves {

    public static final String HREF_REGEXP = "<a\\s+(?:[^>]*?\\s+)?href=\"([^\"]*)";
    private TimeMeasurer measurer;

    @Before
    public void setUp() throws Exception {
        measurer = new TimeMeasurer();
    }

    @Test
    public void testSimpleRLCOnnectionRetrievesWithPatterns() throws Exception {
//        URL url = new URL("http://sudakflat.info/");
        URL url = new URL("http://hadoop.apache.org/docs/r2.7.0/hadoop-yarn/hadoop-yarn-site/DockerContainerExecutor.html");
        URLConnection urlConnection = url.openConnection();

        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String inputLine;
        measurer.start();
        while ((inputLine = in.readLine()) != null) {
            if (inputLine.contains("<a ")) {
//                System.out.println(inputLine);
                System.out.println(parseHref(inputLine));
            }
        }
        measurer.stop();
        System.out.println(measurer.getStopStartDiffTime());
        in.close();
    }

    @Test
    public void testSimpleURLCOnnectionRetrievesWithPatterns() throws Exception {
        Pattern hrefPattern = Pattern.compile(HREF_REGEXP);

//        URL url = new URL("http://sudakflat.info/");
        URL url = new URL("http://hadoop.apache.org/docs/r2.7.0/hadoop-yarn/hadoop-yarn-site/DockerContainerExecutor.html");
        URLConnection urlConnection = url.openConnection();

        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

        String inputLine;
        measurer.start();
        while ((inputLine = in.readLine()) != null) {
            Matcher matcher = hrefPattern.matcher(inputLine);
            if (matcher.find()) {
                System.out.println(matcher.group(1));
            }
        }
        in.close();
        measurer.stop();
        System.out.println("Overall time: " + measurer.getStopStartDiffTime());
    }

    @Test
    public void testURLCOnnectionRetrievesWithPatterns() throws Exception {
        Pattern hrefPattern = Pattern.compile(HREF_REGEXP);

        TimeMeasurer patternMeasurer = new TimeMeasurer();
        TimeMeasurer readLineMeasurer = new TimeMeasurer();

        measurer.start();
//        URL url = new URL("http://stackoverflow.com/questions/1770010/how-do-i-measure-time-elapsed-in-java");
        URL url = new URL("https://www.google.com.ua/advanced_image_search?hl=uk&amp;authuser=0");
        URLConnection urlConnection = url.openConnection();
        System.out.println(measurer.measureAndPrint("URL connection took: "));

        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        System.out.println(measurer.measureAndPrint("BufferedReader took: "));
        String inputLine;
        readLineMeasurer.start();
        patternMeasurer.start();
        while ((inputLine = in.readLine()) != null) {
            System.out.println(readLineMeasurer.measureAndPrint("Readline worked: "));
            patternMeasurer.measure();
            Matcher matcher = hrefPattern.matcher(inputLine);
            System.out.println(patternMeasurer.measureAndPrint("hrefPattern.matcher: "));
            if (matcher.find()) {
                System.out.println(patternMeasurer.measureAndPrint("hrefPattern.find: "));
//                System.out.println(matcher.group(1));
            }
            System.out.println(patternMeasurer.measureAndPrint("hrefPattern.find: "));
            readLineMeasurer.measure();
        }
        patternMeasurer.stop();
        in.close();

        measurer.stop();
        System.out.println("Overall time: " + measurer.getStopStartDiffTime());
        System.out.println("Overall time pattern matcher job: " + patternMeasurer.getStopStartDiffTime());
    }

    @Test
    public void testURLCOnnectionRetrievesWithCustomParsing() throws Exception {

        final String READ_LINE_MEASURE = "readLine";
        TimeMeasurer readLineMeasurer = new TimeMeasurer();

        measurer.start();
        URL url = new URL("http://stackoverflow.com/questions/1770010/how-do-i-stop-time-elapsed-in-java");
        URLConnection urlConnection = url.openConnection();
        System.out.println(measurer.measureAndPrint("URL connection took: "));

        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        System.out.println(measurer.measureAndPrint("BufferedReader took: "));
        String inputLine;
        readLineMeasurer.start();
        while ((inputLine = in.readLine()) != null) {
            readLineMeasurer.measure(READ_LINE_MEASURE);
            System.out.println(measurer.measureAndPrint("ReadLine: "));
            if (inputLine.contains("href")) {
                System.out.println(inputLine);
                System.out.println(measurer.measureAndPrint("Contains: "));
                System.out.println(parseHref(inputLine));
                System.out.println(measurer.measureAndPrint("parseHref: "));
            }
            System.out.println(measurer.measureAndPrint("Contains: "));
        }
        System.out.println(measurer.measureAndPrint("readline + contains: "));
        in.close();

        measurer.stop();
        System.out.println("Overall time: " + measurer.getStopStartDiffTime());

        System.out.println(readLineMeasurer.printReportForMeasureTypes(READ_LINE_MEASURE));
    }

    private String parseHref(String stringWithHref) {
        final char doubleQuote = '"';

        int hrefFirstDoubleQuoteIndex = stringWithHref.indexOf(doubleQuote, stringWithHref.indexOf("href"));
        int hrefSecondDoubleQutoIndex = stringWithHref.indexOf(doubleQuote, hrefFirstDoubleQuoteIndex + 1);

        return stringWithHref.substring(hrefFirstDoubleQuoteIndex + 1, hrefSecondDoubleQutoIndex);
    }

    @Test
    public void testHrefPattern() throws Exception {
        measurer.start();
        Pattern hrefPattern = Pattern.compile(HREF_REGEXP);
        System.out.println(measurer.measureAndPrint("Pattern compile: "));
        String testString = "  <li><a href=\"//stackapps.com\" title=\"apps, scripts, and development with the Stack Exchange API\">Stack Apps</a></li>";

        Matcher matcher = hrefPattern.matcher(testString);
        System.out.println(measurer.measureAndPrint("Get Matcher: "));
        if (matcher.find()) {
            System.out.println(measurer.measureAndPrint("Find: "));
            System.out.println(matcher.group(1));
            System.out.println(measurer.measureAndPrint("Print matcher.group(1): "));
        }

    }
}
