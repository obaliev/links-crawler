package org.home.common.services.util.measure.type;

/**
 * Created by obaliev on 6/21/15.
 */
public abstract class MeasureType {

    protected long currentValue = 0l;

    public long getValue() {
        return currentValue;
    }

    public abstract void update(long time);
}
