package org.home.common.services.content;

import org.home.common.services.content.domain.BasicPageResult;
import org.home.common.services.content.domain.Href;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by obaliev on 6/21/15.
 */
public class BasicRegExpContentProcessor implements ContentProcessor {

    public static final String HREF_REGEXP = "<a\\s+(?:[^>]*?\\s+)?href=\"([^\"]*)";

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicRegExpContentProcessor.class);
    public static final int HREF_LINK_PATTERN_GROUP = 1;

    @Override
    public BasicPageResult processInputStream(InputStream inputStream) {
        BasicPageResult basicPageResult = new BasicPageResult();

        Pattern hrefPattern = Pattern.compile(HREF_REGEXP);

        String inputLine;
        try (BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream))) {
            while ((inputLine = inputReader.readLine()) != null) {
                Matcher matcher = hrefPattern.matcher(inputLine);
                while (matcher.find()) {
                    basicPageResult.addHref(new Href(matcher.group(HREF_LINK_PATTERN_GROUP)));
                }
            }
        } catch (IOException e) {
            LOGGER.error("An error whie processing input stream");
            throw new RuntimeException(e);
        }

        return basicPageResult;
    }
}
