package org.home.common.services.util.measure.type;

/**
 * Created by obaliev on 6/21/15.
 */
public class MaxMeasureType extends MeasureType {
    public MaxMeasureType() {
        currentValue = Long.MIN_VALUE;
    }

    @Override
    public void update(long time) {
        if (currentValue < time) {
            currentValue = time;
        }
    }
}