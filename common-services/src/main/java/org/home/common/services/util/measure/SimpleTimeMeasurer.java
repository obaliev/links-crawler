package org.home.common.services.util.measure;

/**
 * Created by obaliev on 6/21/15.
 */
public class SimpleTimeMeasurer extends BaseTimeMeasurer {

    protected static final long INIT_TIME = 0l;

    private boolean active = false;

    private long startTime = INIT_TIME;
    private long stopTime = INIT_TIME;
    private long tempValue = INIT_TIME;

    public SimpleTimeMeasurer() {
        super(TimeType.NANO);
    }

    public void start() {
        tempValue = getTime();

        if (isActive()) {
            throw new IllegalStateException("Already started");
        }

        startTime = tempValue;
        active = true;
    }

    public void stop() {
        tempValue = getTime();

        if (!isActive()) {
            throw new IllegalStateException("Please start() it first");
        }

        stopTime = tempValue;
        active = false;
    }

    public long getDiff() {
        if (active || stopTime == INIT_TIME) {
            throw new IllegalStateException("Cannot calculate diff: active=" + active + ", stopTime=" + stopTime);
        }

        return stopTime - startTime;
    }

    public boolean isActive() {
        return active;
    }
}
