package org.home.common.services.util.measure.analys;

import org.home.common.services.util.measure.MultipleTimeMeasurer;
import org.home.common.services.util.measure.SimpleTimeMeasurer;
import org.home.common.services.util.measure.type.MaxMeasureType;
import org.home.common.services.util.measure.type.MeasureType;
import org.home.common.services.util.measure.type.MinMeasureType;
import org.home.common.services.util.measure.type.SumAvgMeasureType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by obaliev on 6/21/15.
 */
public class MultipleTimeMeasurerAnalyser {

    private MultipleTimeMeasurer multipleTimeMeasurer;

    private List<MeasureType> measureTypes = new ArrayList<>();

    public MultipleTimeMeasurerAnalyser(MultipleTimeMeasurer multipleTimeMeasurer) {
        this.multipleTimeMeasurer = multipleTimeMeasurer;
    }

    public void analyse() {
        if (measureTypes.isEmpty()) {
            measureTypes = getDefaultListOfMeasureTypes();
        }

        for (SimpleTimeMeasurer timeMeasure : multipleTimeMeasurer.getMeasures()) {
            for (MeasureType measureType : measureTypes) {
                measureType.update(timeMeasure.getDiff());
            }
        }
    }

    public void addMeasureType(MeasureType measureType) {
        measureTypes.add(measureType);
    }

    private List<MeasureType> getDefaultListOfMeasureTypes() {
        List<MeasureType> defaultMeasureTypes = new ArrayList<>();

        defaultMeasureTypes.add(new MinMeasureType());
        defaultMeasureTypes.add(new MaxMeasureType());
        defaultMeasureTypes.add(new SumAvgMeasureType());

        return defaultMeasureTypes;
    }

    public List<MeasureType> getMeasureTypes() {
        return measureTypes;
    }
}
