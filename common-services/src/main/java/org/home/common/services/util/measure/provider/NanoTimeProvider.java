package org.home.common.services.util.measure.provider;

import org.home.common.services.util.measure.TimeType;

/**
 * Created by obaliev on 6/21/15.
 */
public class NanoTimeProvider extends TimeProvider {
    public NanoTimeProvider() {
        super(TimeType.NANO);
    }

    @Override
    public long getTime() {
        return System.nanoTime();
    }
}
