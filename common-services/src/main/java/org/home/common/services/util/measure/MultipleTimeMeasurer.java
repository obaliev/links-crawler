package org.home.common.services.util.measure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by obaliev on 6/21/15.
 */
public class MultipleTimeMeasurer extends BaseTimeMeasurer {

    private SimpleTimeMeasurer tempSimpleTimeMeasurer = new SimpleTimeMeasurer();

    private List<SimpleTimeMeasurer> measures = new ArrayList<>();

    public MultipleTimeMeasurer() {
        super(TimeType.NANO);
    }

    public MultipleTimeMeasurer(TimeType timeType) {
        super(timeType);
    }

    public void start() {
        tempSimpleTimeMeasurer.start();
    }

    public void stop() {
        tempSimpleTimeMeasurer.stop();
        measures.add(tempSimpleTimeMeasurer);
        tempSimpleTimeMeasurer = new SimpleTimeMeasurer();
    }

    public void stopAndStart() {
        stop();
        start();
    }

    public void reset() {
        this.measures = new ArrayList<>();
    }

    public List<SimpleTimeMeasurer> getMeasures() {
        return measures;
    }
}
