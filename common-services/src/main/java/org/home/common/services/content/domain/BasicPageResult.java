package org.home.common.services.content.domain;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by obaliev on 6/21/15.
 */
public class BasicPageResult {

    private URL baseURL;

    private Set<Href> hrefSet;

    public void addHref(Href href) {
        getHrefSet().add(href);
    }

    public Set<Href> getHrefSet() {
        if (hrefSet == null) {
            hrefSet = new HashSet<>();
        }

        return hrefSet;
    }

    public void setHrefSet(Set<Href> hrefSet) {
        this.hrefSet = hrefSet;
    }

    public URL getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(URL baseURL) {
        this.baseURL = baseURL;
    }
}
