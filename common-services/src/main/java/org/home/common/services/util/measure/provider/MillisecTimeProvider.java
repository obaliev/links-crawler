package org.home.common.services.util.measure.provider;

import org.home.common.services.util.measure.TimeType;

/**
 * Created by obaliev on 6/21/15.
 */
public class MillisecTimeProvider extends TimeProvider {
    public MillisecTimeProvider() {
        super(TimeType.MILLISEC);
    }

    @Override
    public long getTime() {
        return System.currentTimeMillis();
    }
}
