package org.home.common.services.util.measure.type;

/**
 * Created by obaliev on 6/21/15.
 */
public class SumAvgMeasureType extends MeasureType {

    int count = 0;

    public SumAvgMeasureType() {
        currentValue = 0l;
    }

    @Override
    public void update(long time) {
        currentValue += time;
        ++count;
    }

    public double avg() {
        return currentValue / count;
    }
}
