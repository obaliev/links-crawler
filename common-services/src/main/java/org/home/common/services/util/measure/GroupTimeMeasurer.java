package org.home.common.services.util.measure;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aleksandr Balev on 6/14/15.
 */
public class GroupTimeMeasurer extends BaseTimeMeasurer {

    private Map<String, MultipleTimeMeasurer> groupMeasures = new HashMap<>();

    public GroupTimeMeasurer() {
        super(TimeType.NANO);
    }

    public GroupTimeMeasurer(TimeType timeType) {
        super(timeType);
    }

    public void start(String groupKey) {
        if (groupMeasures.get(groupKey) == null) {
            groupMeasures.put(groupKey, new MultipleTimeMeasurer(timeProvider.getTimeType()));
        }

        groupMeasures.get(groupKey).start();
    }

    public void stop(String groupKey) {
        groupMeasures.get(groupKey).stop();
    }

    public void reset(String groupKey) {
        groupMeasures.get(groupKey).reset();
    }

    public Map<String, MultipleTimeMeasurer> getGroupMeasures() {
        return groupMeasures;
    }
}
