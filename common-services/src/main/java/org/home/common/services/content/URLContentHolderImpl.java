package org.home.common.services.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by obaliev on 6/21/15.
 */
public class URLContentHolderImpl implements URLContentHolder {

    private final static Logger LOGGER = LoggerFactory.getLogger(URLContentHolderImpl.class);

    private URL basicURL;

    @Override
    public InputStream openInputStream(URL basicURL) {
        this.basicURL = basicURL;

        URLConnection connection = null;
        try {
            connection = this.basicURL.openConnection();
        } catch (IOException e) {
            final String errorMessage = "Cannot established connection with: " + this.basicURL;
            LOGGER.error(errorMessage);
            throw new IllegalStateException(errorMessage, e);
        }

        InputStream inputStream = null;
        try {
            inputStream = connection.getInputStream();
        } catch (IOException e) {
            final String errorMessage = "Cannot get input stream for conenction: " + connection;
//            BufferedReader reader = new BufferedReader(new InputStreamReader(((HttpURLConnection) connection).getErrorStream()));
//            String inputLine;
//            try {
//                while ((inputLine = reader.readLine()) != null) {
//                    System.out.println(inputLine);
//                }
//            } catch (IOException e1) {
//                throw new IllegalStateException(e);
//            }
            throw new IllegalStateException(errorMessage, e);
        }

        return inputStream;
    }
}
