package org.home.common.services.util.measure;

/**
 * Created by obaliev on 6/21/15.
 */
public enum TimeType {
    NANO("ns"), MILLISEC("ms");

    private String simplifiedName;

    TimeType(String simplifiedName) {
        this.simplifiedName = simplifiedName;
    }

    @Override
    public String toString() {
        return simplifiedName;
    }
}
