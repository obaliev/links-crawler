package org.home.common.services.util.measure.report;

import org.home.common.services.util.measure.MultipleTimeMeasurer;
import org.home.common.services.util.measure.analys.MultipleTimeMeasurerAnalyser;
import org.home.common.services.util.measure.provider.TimeProvider;
import org.home.common.services.util.measure.type.MaxMeasureType;
import org.home.common.services.util.measure.type.MeasureType;
import org.home.common.services.util.measure.type.MinMeasureType;
import org.home.common.services.util.measure.type.SumAvgMeasureType;

/**
 * Created by obaliev on 6/21/15.
 */
public class MultipleTimeMeasurerReport {

    private MultipleTimeMeasurerAnalyser multipleTimeMeasurerAnalyser;
    private MultipleTimeMeasurer multipleTimeMeasurer;
    private TimeProvider timeProvider;

    public MultipleTimeMeasurerReport(MultipleTimeMeasurer multipleTimeMeasurer) {
        this.multipleTimeMeasurer = multipleTimeMeasurer;
        this.multipleTimeMeasurerAnalyser = new MultipleTimeMeasurerAnalyser(multipleTimeMeasurer);
        this.timeProvider = multipleTimeMeasurer.getTimeProvider();
    }

    public String generateReport() {
        multipleTimeMeasurerAnalyser.analyse();

        StringBuilder reportBuilder = new StringBuilder();
        for (MeasureType measureType : multipleTimeMeasurerAnalyser.getMeasureTypes()) {
            reportBuilder.append(generateReport(measureType)).append(", ");
        }

        return reportBuilder.toString();
    }

    public String generateReport(MeasureType measureType) {
        if (measureType.getClass() == MinMeasureType.class) {
            return "min: " + measureType.getValue() + " " + timeProvider.getTimeType();
        } else if (measureType.getClass() == MaxMeasureType.class) {
            return "max: " + measureType.getValue() + " " + timeProvider.getTimeType();
        } else if (measureType.getClass() == SumAvgMeasureType.class) {
            return "sum: " + measureType.getValue() + " " + timeProvider.getTimeType()
                    + ", avg: " + ((SumAvgMeasureType) measureType).avg() + " " + timeProvider.getTimeType();
        } else {
            throw new IllegalStateException("Report for Measure Type: " + measureType.getClass() + " is not supported");
        }
    }
}
