package org.home.common.services.content;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by obaliev on 6/21/15.
 */
public interface URLContentHolder {

    InputStream openInputStream(URL basicURL);
}
