package org.home.common.services.util.measure.provider;

import org.home.common.services.util.measure.TimeType;

/**
 * Created by obaliev on 6/21/15.
 */
public abstract class TimeProvider {
    protected TimeType timeType;

    public TimeProvider(TimeType timeType) {
        this.timeType = timeType;
    }

    public TimeType getTimeType() {
        return timeType;
    }

    public abstract long getTime();
}