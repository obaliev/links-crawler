package org.home.common.services.util.measure;

import org.home.common.services.util.measure.provider.MillisecTimeProvider;
import org.home.common.services.util.measure.provider.NanoTimeProvider;
import org.home.common.services.util.measure.provider.TimeProvider;

/**
 * Created by obaliev on 6/21/15.
 */
public abstract class BaseTimeMeasurer {

    protected TimeProvider timeProvider;

    public BaseTimeMeasurer(TimeType timeType) {
        if (timeType.equals(TimeType.NANO)) {
            this.timeProvider = new NanoTimeProvider();
        } else if (timeType.equals(TimeType.MILLISEC)) {
            this.timeProvider = new MillisecTimeProvider();
        } else {
            throw new IllegalArgumentException(timeType + " is not supported");
        }
    }

    protected long getTime() {
        return timeProvider.getTime();
    }

    public TimeProvider getTimeProvider() {
        return timeProvider;
    }
}
