package org.home.common.services.util.measure;

import org.home.common.services.util.measure.provider.MillisecTimeProvider;
import org.home.common.services.util.measure.provider.NanoTimeProvider;
import org.home.common.services.util.measure.provider.TimeProvider;
import org.home.common.services.util.measure.type.MaxMeasureType;
import org.home.common.services.util.measure.type.MeasureType;
import org.home.common.services.util.measure.type.MinMeasureType;
import org.home.common.services.util.measure.type.SumAvgMeasureType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by Aleksandr Balev on 6/14/15.
 */
public class TimeMeasurer {

    private boolean active = false;
    private long lastMeasuredTime = 0l;
    private long startTime = 0l;
    private long stopTime = 0l;

    private long tempValue = 0l;
    private long tempLastDiff = 0l;

    private long count;

    HashMap<String, List<MeasureType>> store = new HashMap<>();

    private TimeProvider timeProvider;

    public TimeMeasurer(TimeType timeType) {
        if (timeType.equals(TimeType.NANO)) {
            this.timeProvider = new NanoTimeProvider();
        } else if (timeType.equals(TimeType.MILLISEC)) {
            this.timeProvider = new MillisecTimeProvider();
        } else {
            throw new IllegalArgumentException(timeType + " is not supported");
        }
    }

    public TimeMeasurer() {
        this(TimeType.NANO);
    }

    public void start() {
        long currentValue = getTime();

        if (isActive()) {
            throw new IllegalStateException("Already started");
        }

        startTime = lastMeasuredTime = currentValue;
        active = true;
    }

    public void stop() {
        long currentValue = getTime();

        if (!isActive()) {
            throw new IllegalStateException("Please start() it first");
        }

        stopTime = currentValue;
        active = false;
    }

    public boolean isActive() {
        return active;
    }

    public void measure() {
        tempValue = getTime();
        tempLastDiff = tempValue - lastMeasuredTime;
        lastMeasuredTime = tempValue;
        ++count;
    }

    public void measure(String key) {
        measure();

        if (store.get(key) == null) {
            store.put(key, setDefaultListOfMeasureTypes(new ArrayList<>()));
        }

        for (MeasureType measureType : store.get(key)) {
            measureType.update(tempLastDiff);
        }
    }

    private List<MeasureType> setDefaultListOfMeasureTypes(List<MeasureType> list) {
        if (list == null) {
            throw new IllegalArgumentException("List for loading default Measure Types cannot be null");
        }

        list.add(new MinMeasureType());
        list.add(new MaxMeasureType());
        list.add(new SumAvgMeasureType());

        return list;
    }

    public void addMeasureType(String key, MeasureType measureType) {
        if (store.get(key) == null) {
            store.put(key, setDefaultListOfMeasureTypes(new ArrayList<>()));
        }

        store.get(key).add(measureType);
    }

    public String printReportForMeasureType(String key, Class measureTypeClass) {
        List<MeasureType> measureTypes = store.get(key);

        if (measureTypes == null) {
            throw new IllegalArgumentException("No existing measures for key: " + key);
        }

        MeasureType discoveredMeasureType = null;
        for (MeasureType measureType : measureTypes) {
            if (measureType.getClass() == measureTypeClass) {
                discoveredMeasureType = measureType;
            }
        }

        return "Report for " + key + ": " + printReportForMeasureType(discoveredMeasureType);
    }

    public String printReportForMeasureType(MeasureType measureType) {
        if (measureType == null) {
            throw new IllegalArgumentException("Measure Type cannot be null");
        }

        if (measureType.getClass() == MinMeasureType.class) {
            return "min: " + measureType.getValue() + " " + timeProvider.getTimeType();
        } else if (measureType.getClass() == MaxMeasureType.class) {
            return "max: " + measureType.getValue() + " " + timeProvider.getTimeType();
        } else if (measureType.getClass() == SumAvgMeasureType.class) {
            return "sum: " + measureType.getValue() + " " + timeProvider.getTimeType()
                    + ", avg: " + (measureType.getValue() / count) + " " + timeProvider.getTimeType();
        } else {
            throw new IllegalStateException("Report for Measure Type: " + measureType.getClass() + " is not supported");
        }
    }

    public String printReportForMeasureTypes(String key) {
        List<MeasureType> measureTypes = store.get(key);

        if (measureTypes == null) {
            throw new IllegalArgumentException("No existing measures for key: " + key);
        }

        StringBuilder builder = new StringBuilder("Report for " + key + ": ");
        for (MeasureType measureType : measureTypes) {
            builder.append(printReportForMeasureType(measureType)).append(", ");
        }

        return builder.toString();
    }

    public String measureAndPrint(String prefixMessage) {
        measure();

        return prefixMessage + tempLastDiff + " " + timeProvider.getTimeType();
    }

    public String measureAndPrint() {
        measure();

        return tempLastDiff + " " + timeProvider.getTimeType();
    }

    public long getStopStartDiffTime() {
        return stopTime - startTime;
    }

    private long getTime() {
        return timeProvider.getTime();
    }

    public Set<String> getStoreKeys() {
        return store.keySet();
    }
}
