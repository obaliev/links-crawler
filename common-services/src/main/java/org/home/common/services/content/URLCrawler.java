package org.home.common.services.content;

import org.home.common.services.content.domain.BasicPageResult;
import org.home.common.services.content.domain.Href;
import org.home.common.services.util.measure.GroupTimeMeasurer;
import org.home.common.services.util.measure.report.MultipleTimeMeasurerReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by obaliev on 6/21/15.
 */
public class URLCrawler {

    private static final Logger LOGGER = LoggerFactory.getLogger(URLCrawler.class);

    private URLContentHolder urlContentHolder = new URLContentHolderImpl();
    private ContentProcessor contentProcessor = new BasicRegExpContentProcessor();

    private static GroupTimeMeasurer groupTimeMeasurer = new GroupTimeMeasurer();

    private static final int INIT_DEPTH = 3;

    public static void main(String[] args) throws Exception {
        new URLCrawler().crawl("http://euro.com.ua/ukrainskii-basketbol/220615-lishchuk-pokinul-valensiyu");

        for (String key : groupTimeMeasurer.getGroupMeasures().keySet()) {
            MultipleTimeMeasurerReport multipleTimeMeasurerReport =
                    new MultipleTimeMeasurerReport(groupTimeMeasurer.getGroupMeasures().get(key));

            System.out.println(key + ": " + multipleTimeMeasurerReport.generateReport());
        }
    }

    public void crawl(String url) throws Exception {
        URL baseURL = new URL(url);

        groupTimeMeasurer.start("BASE");
        processResult(baseURL, INIT_DEPTH);
        groupTimeMeasurer.stop("BASE");
    }


    private void processResult(URL baseURL, int depth) throws MalformedURLException {
        if (depth == 0) {
            return;
        }

        groupTimeMeasurer.start(baseURL.toString());
        BasicPageResult basicPageResult = contentProcessor.processInputStream(urlContentHolder.openInputStream(baseURL));

        for (Href href : basicPageResult.getHrefSet()) {
            String resultHref = printFullHref(baseURL, href.getValue());
            printResult(resultHref, INIT_DEPTH - depth);
            processResult(new URL(resultHref), depth - 1);
        }
        groupTimeMeasurer.stop(baseURL.toString());
    }

    private void printResult(String message, int otstyp) {
        String intend = "";
        for (int i = 0; i < otstyp; i++) {
            intend += "- ";
        }
        System.out.println(intend + message);
    }

    private String printFullHref(URL baseURL, String localURLName) {
        String fullURLName = null;

        URL localURL = null;
        try {
            localURL = new URL(localURLName);
        } catch (MalformedURLException e) {
            LOGGER.warn("Cannot transform to url: " + localURLName);

            String slash = "";
            if (!baseURL.toString().endsWith("/") && !localURLName.startsWith("/")) {
                slash = "/";
            }

            fullURLName = baseURL.toString() + slash + localURLName;
        }

        if (fullURLName == null) {
            fullURLName = localURLName.toString();
        }

        return fullURLName;
    }
}
