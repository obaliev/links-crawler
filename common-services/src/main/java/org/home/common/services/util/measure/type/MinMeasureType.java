package org.home.common.services.util.measure.type;

/**
 * Created by obaliev on 6/21/15.
 */
public class MinMeasureType extends MeasureType {
    public MinMeasureType() {
        currentValue = Long.MAX_VALUE;
    }

    @Override
    public void update(long time) {
        if (currentValue > time) {
            currentValue = time;
        }
    }

}