package org.home.common.services.content;

import org.home.common.services.content.domain.BasicPageResult;

import java.io.InputStream;

/**
 * Created by obaliev on 6/21/15.
 */
public interface ContentProcessor {

    BasicPageResult processInputStream(InputStream inputStream);
}
